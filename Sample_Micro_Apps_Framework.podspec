Pod::Spec.new do |spec|
	spec.name = 'Sample_Micro_Apps_Framework'
	spec.version = '0.1.0'
	spec.summary = 'Just Sample Micro App'
	spec.homepage = 'https://wahyudimas@bitbucket.org/wahyudimas/sample_micro_apps_framework'
	spec.license = { type: 'MIT' }
	spec.authors = { "Wahyudimas H. R. S." => 'wahyudimas@icehousecorp.com' }

	spec.platform = :ios, '9.0'
	spec.requires_arc = true
	spec.source = { git: 'https://wahyudimas@bitbucket.org/wahyudimas/sample_micro_apps_framework.git', tag: 'v#{spec.version}', submodules: true }

	spec.source_files = 'Sample_Micro_Apps_Framework/**/*.{h,m,swift,xib,modulemap}'
	spec.preserve_paths = 'Sample_Micro_Apps_Framework/module.modulemap'
	spec.xcconfig = { 'HEADER_SEARCH_PATHS' => '"$(SDKROOT)/usr/include/libxml2"', 'SWIFT_INCLUDE_PATHS' => '$(PODS_ROOT)/Sample_Micro_Apps_Framework' }

	spec.dependency 'MPMobileFramework'
end
