//
//  DFCrashReport.h
//  APMobileRuntime
//
//  Created by liangbao.llb on 12/30/14.
//  Copyright (c) 2014 Alipay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <mach/mach.h>

/**
 *  自定义crash日志接口使用说明
 *  
 *  1）调用 +[DFCrashReport addCrashReportDelegate:] 设置代理
 *  2）遵循 DFCrashReportDelegate 属性协议，并且手动 @synthesize crashReportMsg; （属性协议不支持auto synthesize）
 *  3）实现 DFCrashReportDelegate 协议中的 crashReportMsg 的getter方法，来提供自定义crash日志内容
 *
 */

/**
 *  启动APCrashReporter服务
 */
void enable_crash_reporter_service (void);

@protocol DFCrashReportDelegate <NSObject>

@property (nonatomic,strong)NSString *crashReportMsg;

@end

@interface DFCrashReport : NSObject

/**
 *  上传crash日志
 */
+ (void)uploadCrashReport;

/**
 *  获取内存中所有的VC
 *
 *  @return 返回包含所有VC的字符串
 */
+(NSString *) allAliveVc;

/**
 *  开启Low Memory Crash Log收集服务
 */
+ (void)enableMemoryCrashReport;

/**
 *  为自定义crash日志设置代理
 *
 *  @param delegate 代理对象
 */
+ (void)addCrashReportDelegate:(id<DFCrashReportDelegate>)delegate;

+ (NSString *)generateMonitorLogWithThread:(thread_t) tid;
@end
