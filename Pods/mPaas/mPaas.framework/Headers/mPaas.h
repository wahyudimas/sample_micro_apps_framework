//
//  mPaas.h
//  mPaas
//
//  Created by shenmo on 10/23/15.
//  Copyright © 2015 Alibaba. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for mPaas.
FOUNDATION_EXPORT double mPaasVersionNumber;

//! Project version string for mPaas.
FOUNDATION_EXPORT const unsigned char mPaasVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <mPaas/PublicHeader.h>

#import "mPaasContext.h"