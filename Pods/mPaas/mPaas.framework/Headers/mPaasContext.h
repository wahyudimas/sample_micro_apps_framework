//
//  mPaasContext.h
//  mPaasContext
//
//  Created by shenmo on 12/16/14.
//  Copyright (c) 2014 Alipay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "mPaasAppInterface.h"
#import "mPaasContextInterface.h"
#import "MPSettingService.h"
#import "APMobileIdentifier.h"
#import "DTDeviceInfo.h"
#import "APLanguage.h"

/**
 *  第三方app请在应用启动后第一时间调用mPaasInit方法，并将自己实现mPaasAppInterface协议的类传给mPaas
 */
#ifdef __cplusplus
extern "C" {
#endif // __cplusplus
    
    BOOL mPaasInit(NSString* appKey, Class appInterfaceClass);
    id<mPaasContext> mPaas();
    
#ifdef __cplusplus
}
#endif // __cplusplus