//
//  mPaasContextInterface.h
//  mPaasInternal
//
//  Created by shenmo on 2/10/15.
//  Copyright (c) 2015 shenmo. All rights reserved.
//

@protocol mPaasContext <NSObject>

- (NSString*)appKey;
- (NSTimeInterval)initializationTimestamp;

- (id<mPaasAppInterface>)appInterfaceInstance;

- (id)serviceWithName:(NSString*)name;

- (void)registerMethod:(NSString*)method service:(NSString*)service;
- (void)unregisterMethod:(NSString*)method;
- (id)executeMethod:(NSString*)method arguments:(NSDictionary*)arguments;

@end
