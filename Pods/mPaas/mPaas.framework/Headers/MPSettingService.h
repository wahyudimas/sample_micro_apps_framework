//
//  MPSettingService.h
//  mPaasInternal
//
//  Created by shenmo on 5/25/15.
//  Copyright (c) 2015 shenmo. All rights reserved.
//

@protocol MPSettingService <NSObject>

@property (nonatomic, strong) NSMutableDictionary* settings;

@end