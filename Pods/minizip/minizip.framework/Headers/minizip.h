//
//  minizip.h
//  minizip
//
//  Created by jenkins on 7/4/16.
//  Copyright © 2016 alibaba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <minizip/zip.h>
#import <minizip/unzip.h>

//! Project version number for minizip.
FOUNDATION_EXPORT double minizipVersionNumber;

//! Project version string for minizip.
FOUNDATION_EXPORT const unsigned char minizipVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <minizip/PublicHeader.h>


