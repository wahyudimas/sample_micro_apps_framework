//
//  NSDate+DTJsonString.m
//  MobileFoundation
//
//  Created by WenBi on 13-4-12.
//  Copyright (c) 2013年 Alipay. All rights reserved.
//

#import "NSDate+DTJsonString.h"

@implementation NSDate (DTJsonString)

+ (NSDate *)dateFromJSONString:(NSString *)string
{
	NSTimeInterval timeInterval = [string doubleValue] / 1000;
	return [NSDate dateWithTimeIntervalSince1970:timeInterval];
}

- (NSString *)toJSONString
{
	NSTimeInterval timeInterval = [self timeIntervalSince1970];
	NSString *dateString = [NSString stringWithFormat:@"%f", timeInterval];
	NSInteger i = [dateString rangeOfString:@"."].location;
	if (i != NSNotFound) {
		if (i + 4 < [dateString length]) {
			dateString = [dateString substringToIndex:i + 4];
		}
		dateString = [dateString stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@""];
	}
	return dateString;
}

@end
