//
//  DTNumber.m
//  MobileFoundation
//
//  Created by WenBi on 13-4-19.
//  Copyright (c) 2013年 Alipay. All rights reserved.
//

#import "DTNumber.h"

@interface DTNumber ()

@property(nonatomic, copy) NSNumber *number;

@end

@implementation DTNumber

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    if (self.number)
        [aCoder encodeObject:self.number forKey:@"value"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init])
    {
        id value = [aDecoder decodeObjectForKey:@"value"];
        if ([value isKindOfClass:[NSNumber class]])
            self.number = value;
    }
    return self;
}

- (id)initWithChar:(char)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithChar:value];
	}
	return self;
}

- (id)initWithUnsignedChar:(unsigned char)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithUnsignedChar:value];
	}
	return self;
}

- (id)initWithShort:(short)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithShort:value];
	}
	return self;
}

- (id)initWithUnsignedShort:(unsigned short)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithUnsignedShort:value];
	}
	return self;
}

- (id)initWithInt:(int)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithInt:value];
	}
	return self;
}

- (id)initWithUnsignedInt:(unsigned int)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithUnsignedInt:value];
	}
	return self;
}

- (id)initWithLong:(long)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithLong:value];
	}
	return self;
}

- (id)initWithUnsignedLong:(unsigned long)value;
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithUnsignedLong:value];
	}
	return self;
}

- (id)initWithLongLong:(long long)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithLongLong:value];
	}
	return self;
}

- (id)initWithUnsignedLongLong:(unsigned long long)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithUnsignedLongLong:value];
	}
	return self;
}

- (id)initWithFloat:(float)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithFloat:value];
	}
	return self;
}

- (id)initWithDouble:(double)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithDouble:value];
	}
	return self;
}

- (id)initWithBool:(BOOL)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithBool:value];
	}
	return self;
}

- (id)initWithInteger:(NSInteger)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithInteger:value];
	}
	return self;
}

- (id)initWithUnsignedInteger:(NSUInteger)value
{
	self = [super init];
	if (self) {
		self.number = [NSNumber numberWithUnsignedInteger:value];
	}
	return self;
}

- (char)charValue { return [self.number charValue]; }
- (unsigned char)unsignedCharValue { return [self.number unsignedCharValue]; }
- (short)shortValue { return [self.number shortValue]; }
- (unsigned short)unsignedShortValue { return [self.number unsignedShortValue]; }
- (int)intValue { return [self.number intValue]; }
- (unsigned int)unsignedIntValue { return [self.number unsignedIntValue]; }
- (long)longValue { return [self.number longValue]; }
- (unsigned long)unsignedLongValue { return [self.number unsignedLongValue]; }
- (long long)longLongValue { return [self.number longLongValue]; }
- (unsigned long long)unsignedLongLongValue { return [self.number unsignedLongLongValue]; }
- (float)floatValue { return [self.number floatValue]; }
- (double)doubleValue { return [self.number doubleValue]; }
- (BOOL)boolValue { return [self.number boolValue]; }
- (NSInteger)integerValue { return [self.number integerValue]; }
- (NSUInteger)unsignedIntegerValue { return [self.number unsignedIntegerValue]; }

- (NSString *)stringValue { return [self.number stringValue]; }

- (NSComparisonResult)compare:(DTNumber *)otherNumber { return [self.number compare:otherNumber.number]; }

- (BOOL)isEqualToNumber:(DTNumber *)number { return [self.number isEqualToNumber:number.number]; }

- (NSString *)description { return [self.number description]; }

- (BOOL)isEqual:(id)object
{
    if (self == object) {
        return YES;
    }
    
    if ([object isKindOfClass:[DTNumber class]]) {
        return [self.number isEqual:[(DTNumber *)object number]];
    }
    return NO;
}

@end

@implementation DTBoolean

+ (DTBoolean *)booleanWithObject:(id)object
{
	if (object == nil) {
		return nil;
	}
	
	if ([object isKindOfClass:[NSNumber class]] || [object isKindOfClass:[NSString class]]) {
		return [[DTBoolean alloc] initWithBool:[object boolValue]];
	}
	
	return nil;
}

@end

@implementation DTByte

+ (DTByte *)byteWithObject:(id)object
{
	if (object == nil) {
		return nil;
	}
	
	if ([object isKindOfClass:[NSNumber class]] || [object isKindOfClass:[NSString class]]) {
		return [[DTByte alloc] initWithUnsignedChar:[object intValue]];
	}
	
	return nil;
}

@end

@implementation DTCharacter

+ (DTCharacter *)characterWithObject:(id)object
{
	if (object == nil) {
		return nil;
	}
	
	if ([object isKindOfClass:[NSNumber class]] || [object isKindOfClass:[NSString class]]) {
		return [[DTCharacter alloc] initWithUnsignedShort:[object intValue]];
	}
	
	return nil;
}

@end

@implementation DTDouble

+ (DTDouble *)doubleWithObject:(id)object
{
	if (object == nil) {
		return nil;
	}

	if ([object isKindOfClass:[NSNumber class]] || [object isKindOfClass:[NSString class]]) {
		return [[DTDouble alloc] initWithDouble:[object doubleValue]];
	}
	
	return nil;
}

@end

@implementation DTFloat

+ (DTFloat *)floatWithObject:(id)object
{
	if (object == nil) {
		return nil;
	}
	
	if ([object isKindOfClass:[NSNumber class]] || [object isKindOfClass:[NSString class]]) {
		return [[DTFloat alloc] initWithFloat:[object floatValue]];
	}
	
	return nil;
}

@end

@implementation DTInteger

+ (DTInteger *)integerWithObject:(id)object
{
	if (object == nil) {
		return nil;
	}
	
	if ([object isKindOfClass:[NSString class]] || [object isKindOfClass:[NSNumber class]]) {
		return [[DTInteger alloc] initWithInt:[object intValue]];
	}
	
	return nil;
}

@end

@implementation DTLong

+ (DTLong *)longWithObject:(id)object
{
	if (object == nil) {
		return nil;
	}
	
	if ([object isKindOfClass:[NSNumber class]] || [object isKindOfClass:[NSString class]]) {
		return [[DTLong alloc] initWithLongLong:[object longLongValue]];
	}
	
	return nil;
}

@end

@implementation DTShort

+ (DTShort *)shortWithObject:(id)object
{
	if (object == nil) {
		return nil;
	}
	
	if ([object isKindOfClass:[NSString class]] || [object isKindOfClass:[NSNumber class]]) {
		return [[DTShort alloc] initWithShort:[object intValue]];
	}
	
	return nil;
}

@end