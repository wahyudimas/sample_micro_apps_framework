//
//  DTJsonHelper.m
//  MobileFoundation
//
//  Created by WenBi on 13-4-12.
//  Copyright (c) 2013年 Alipay. All rights reserved.
//

#import "DTJsonHelper.h"

@implementation DTJsonHelper

+ (Class)elementClassForContainer:(NSString *)name ofObject:(id)object
{
	name = [NSString stringWithFormat:@"%@ElementClass", name];
	SEL selector = NSSelectorFromString(name);
	Class cls = NULL;
	@try {
		cls = [[object class] performSelector:selector];
	}
	@catch (NSException *exception) {
		NSLog(@"%@", exception);
	}
	return cls;
}

+ (NSString *)typeStringForProperty:(objc_property_t)property
{
	const char *attr = property_getAttributes(property);
	const char *comma = attr;
	char buffer[64] = { 0 };
	int i = 0;
	
	// Skips the first letter: 'T'.
	//
	++comma;
	
	while (*comma && *comma != ',' && comma - attr < 64) {
		buffer[i++] = *comma++;
	}
	return [NSString stringWithUTF8String:buffer];
}

@end
