//
//  APStringUtils.m
//  APJSONKit
//
//  Created by WenBi on 14-1-14.
//  Copyright (c) 2014年 WenBi. All rights reserved.
//

#import "APStringUtils.h"

void APMutableStringRemoveLastComma(NSMutableString *string)
{
    if ([string length] > 0) {
		NSRange range = NSMakeRange(string.length - 1, 1);
		NSString *substring = [string substringWithRange:range];
		if ([substring isEqualToString:@","]) {
			[string replaceCharactersInRange:range withString:@""];
		}
	}
}
