//
//  DTJsonEncoder.m
//  RPC
//
//  Created by WenBi on 13-2-27.
//  Copyright (c) 2013年 Alipay. All rights reserved.
//

#import "APJSONKit.h"
#import "DTNumber.h"
#import "APStringUtils.h"
#import "NSDate+DTJsonString.h"
#import "DTJsonHelper.h"
#include <objc/runtime.h>

@interface DTJsonEncoder ()

@property(nonatomic, strong) NSMutableString *JSONRepresentation;

@end

@implementation DTJsonEncoder

static NSMutableCharacterSet *kEscapeChars;

+ (void)initialize {
	kEscapeChars = [NSMutableCharacterSet characterSetWithRange: NSMakeRange(0,32)];
	[kEscapeChars addCharactersInString: @"\"\\"];
}


+ (DTJsonEncoder *)encoder
{
	return [[DTJsonEncoder alloc] init];
}

- (id)init
{
	self = [super init];
	if (self) {
		//
		// 因为要不断追加字符串，所以预分配 128 个字节的内存空间，以提高内存效率。
		//
		self.JSONRepresentation = [NSMutableString stringWithCapacity:128];
	}
	return self;
}

- (void)encodeArray:(NSArray *)array
{
	NSAssert([array isKindOfClass:[NSArray class]], nil);
	
	// 数组开始标记。
	[_JSONRepresentation appendString:@"["];
	
	for (id obj in array) {
		if ([obj isKindOfClass:[NSString class]]) {
            
            NSString *str = [self jsonValueFrom:(NSString*)obj];
			[_JSONRepresentation appendFormat:@"%@", str];
		}
		else if ([obj isKindOfClass:[NSNumber class]] || [obj isKindOfClass:[DTNumber class]]) {
			[_JSONRepresentation appendFormat:@"%@,", obj];
		}
		else if ([obj isKindOfClass:[NSDate class]]) {
			[_JSONRepresentation appendFormat:@"%@,", [obj toJSONString]];
		}
		else if ([obj isKindOfClass:[NSNull class]]) {
			[_JSONRepresentation appendString:@"null,"];
		}
		else {
			[self encodeObject:obj];
            [_JSONRepresentation appendString:@","];
		}
	}
	
	//
	// 循环结束后，删除最后一个逗号（,）
	//
    APMutableStringRemoveLastComma(_JSONRepresentation);
	
	// 数组结束标记。
	[_JSONRepresentation appendString:@"]"];
}

- (void)encodeDictionary:(NSDictionary *)dict
{
	NSAssert([dict isKindOfClass:[NSDictionary class]], nil);
	
	// 字典开始标记。
	[_JSONRepresentation appendString:@"{"];
	
	for (NSString *key in [dict allKeys]) {
		[_JSONRepresentation appendFormat:@"\"%@\":", key];
		id obj = [dict objectForKey:key];
		if ([obj isKindOfClass:[NSString class]]) {
            NSString *str = [self jsonValueFrom:(NSString *)obj];
            [_JSONRepresentation appendString:str];
		}
		else if ([obj isKindOfClass:[NSNumber class]]) {
			[_JSONRepresentation appendFormat:@"%@,", obj];
		}
		else if ([obj isKindOfClass:[NSDate class]]) {
			[_JSONRepresentation appendFormat:@"%@,", [obj toJSONString]];
		}
		else {
			[self encodeObject:obj];
			[_JSONRepresentation appendString:@","];
		}
	}
	
	// 循环结束后，删除最后一个逗号（,）
    APMutableStringRemoveLastComma(_JSONRepresentation);

	// 字典结束标记。
	[_JSONRepresentation appendString:@"}"];
}

- (void)encodeObject:(id)obj ofClass:(Class)cls recusive:(BOOL)recusive
{
	NSAssert(obj != nil, @"The parameter must no be nil");
	
	if (!recusive) {
		[_JSONRepresentation appendString:@"{"];
	}
	
	//
	// 如果指定的类型有父类，则先用递归的方式，对父类中的属性进行序列化。
	//
	Class superClass = class_getSuperclass(cls);
	if (superClass && superClass != [NSObject class] && superClass != [NSDate class]) {
		[self encodeObject:obj ofClass:superClass recusive:YES];
	}
	
	//
	// 获取指定类型中的属性列表及属性个数。
	//
	uint32_t count = 0;
	objc_property_t *propertyList = class_copyPropertyList(cls, &count);
	
	for (NSInteger i = 0; i < count; ++i) {
		objc_property_t property = propertyList[i];
		NSString *name = [NSString stringWithUTF8String:property_getName(property)];
		id value = nil;
		
		@try {
			value = [obj valueForKey:name];
		}
		@catch (NSException *ex) {
		}
		
		if (value == nil || value == [NSNull null]) {
			continue;
		}
		
//		NSString *typeString = [DTJsonHelper typeStringForProperty:property];
//		if ([typeString isEqualToString:@"c"]) {
//			name = [[self class] normalizePropertyName:name];
//			[_JSONRepresentation appendFormat:@"\"%@\":", name];
//		}
		
		[_JSONRepresentation appendFormat:@"\"%@\":", name];
		
		if ([value isKindOfClass:[NSString class]]) {
            
            NSString *formatValue = [self jsonValueFrom:value];
            [_JSONRepresentation appendString:formatValue];
		}
		else if ([value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[DTNumber class]]) {
			[_JSONRepresentation appendFormat:@"%@,", value];
		}
		else if ([value isKindOfClass:[NSDate class]]) {
			[_JSONRepresentation appendFormat:@"%@,", [(NSDate *)value toJSONString]];
		}
		else {
			[self encodeObject:value];
			[_JSONRepresentation appendString:@","];
		}
		
	}

	if (!recusive) {
        APMutableStringRemoveLastComma(_JSONRepresentation);
		[_JSONRepresentation appendString:@"}"];
	}
		

	if (propertyList) {
		free(propertyList);
	}
}

- (NSString *)encodeObject:(id)obj
{
	if (obj) {
		if ([obj isKindOfClass:[NSArray class]]) {
			// 序列化数组
			//
			[self encodeArray:obj];
		}
		else if ([obj isKindOfClass:[NSDictionary class]]) {
			// 序列化字典
			//
			[self encodeDictionary:obj];
		}
		else if ([obj isKindOfClass:[NSDate class]]) {
			
		}
		else {
			// 序列化对象
			//
			[self encodeObject:obj ofClass:[obj class] recusive:NO];
		}
	}
	return _JSONRepresentation;
}

+ (NSString *)normalizePropertyName:(NSString *)name
{
	if (name.length >= 3 && [name hasPrefix:@"is"]) {
		unichar ch = [name characterAtIndex:2];
		if (ch >= 'A' && ch <= 'Z') {
			name = [NSString stringWithFormat:@"%c%@", ch + 32, (name.length > 3 ? [name substringFromIndex:3] : @"")];
		}
	}
	return name;
}

- (NSString *)jsonValueFrom:(NSString*)fragment{
    
    NSMutableString *json = [NSMutableString stringWithCapacity:100];
        
    NSRange esc = [fragment rangeOfCharacterFromSet:kEscapeChars];
    if ( !esc.length ) {
        // No special chars -- can just add the raw string:
        //[json appendString:fragment];
        [json appendFormat:@"\"%@\",", fragment];
        
    } else {
        [json appendString:@"\""];

        NSUInteger length = [fragment length];
        for (NSUInteger i = 0; i < length; i++) {
            unichar uc = [fragment characterAtIndex:i];
            switch (uc) {
                case '"':   [json appendString:@"\\\""];       break;
                case '\\':  [json appendString:@"\\\\"];       break;
                case '\t':  [json appendString:@"\\t"];        break;
                case '\n':  [json appendString:@"\\n"];        break;
                case '\r':  [json appendString:@"\\r"];        break;
                case '\b':  [json appendString:@"\\b"];        break;
                case '\f':  [json appendString:@"\\f"];        break;
                default:
                    if (uc < 0x20) {
                        [json appendFormat:@"\\u%04x", uc];
                    } else {
                        CFStringAppendCharacters((CFMutableStringRef)json, &uc, 1);
                    }
                    break;
                    
            }
        }
        [json appendString:@"\","];
    }
    
    return json;
}

@end
