//
//  DTJsonDecoder.m
//  RPC
//
//  Created by WenBi on 13-2-27.
//  Copyright (c) 2013年 Alipay. All rights reserved.
//

#import "APJSONKit.h"
#import "DTJsonHelper.h"
#import "DTNumber.h"
#import <objc/runtime.h>

NSString * const NSClassCastExpcetion = @"NSClassCastExpcetion";

static Class APClassFromString(NSString *string);

@interface DTJsonDecoder ()

@end

@implementation DTJsonDecoder

+ (DTJsonDecoder *)decoder;
{
	return [[DTJsonDecoder alloc] init];
}

- (id)decodeWithClass:(Class)cls elementClass:(Class)elementClass fromJSONString:(NSString *)string
{
	if (cls == NULL || string == nil || [string length] == 0) {
		return nil;
	}
		
	// 把输入的字符串解析到一个字典对象中。
	//
	NSDictionary *dict = [string JSONValue];
	if (dict == nil) {
		[NSException raise:NSGenericException format:@"The specified string is not a valid JSON string"];
	}
	
	return [self decodeWithClass:cls elementClass:elementClass fromJSONObject:dict];
}

- (id)decodeWithClassName:(NSString *)className elementClass:(Class)elementClass fromJSONObject:(id)json
{
    Class cls;
    if ([className isEqualToString:@"B"]) {
        cls = [DTBoolean class];
    }
    else {
        cls = APClassFromString(className);
    }
	return [self decodeWithClass:cls elementClass:elementClass fromJSONObject:json];
}

- (id)decodeWithClass:(Class)cls elementClass:(Class)elementClass fromJSONObject:(id)json
{
	if (cls == NULL || json == nil) {
		return nil;
	}
		
	id resultObject = nil;
	
	do {
		
		// 期望返回一个字典（Map）对象。
		//
		if (cls == [NSDictionary class]) {
            if (elementClass) {
                resultObject = [self decodeDictionaryWithElementClass:elementClass fromJSONObject:json];
            }
            else {
                resultObject = json;
            }
			break;
		}
		
		// 期望返回一个数组。
		//
		if (cls == [NSArray class]) {
			resultObject = [self decodeArrayWithElementClass:elementClass fromJSONObject:json];
			break;
		}
        
        if (cls == [NSString class]) {
            resultObject = json;
            break;
        }
		
		// 期望返回一个字符串。
		//
		if (cls == [NSString class]) {
			if ([json isKindOfClass:[NSString class]]) {
				resultObject = json;
			}
			break;
		}
		
		if ([cls isSubclassOfClass:[NSDate class]]) {
			if ([json isKindOfClass:[NSNumber class]]) {
				NSTimeInterval timeInterval = [json doubleValue] / 1000;
				resultObject =  [NSDate dateWithTimeIntervalSince1970:timeInterval];
			}
			else if ([json isKindOfClass:[NSDate class]]) {
				resultObject = json;
			}
			break;
		}
		
		if ([cls isSubclassOfClass:[DTBoolean class]]) {
			resultObject = [DTBoolean booleanWithObject:json];
			break;
		}

		if ([cls isSubclassOfClass:[DTByte class]]) {
			resultObject = [DTByte byteWithObject:json];
			break;
		}

		if ([cls isSubclassOfClass:[DTCharacter class]]) {
			resultObject = [DTCharacter characterWithObject:json];
			break;
		}
		
		if ([cls isSubclassOfClass:[DTShort class]]) {
			resultObject = [DTShort shortWithObject:json];
			break;
		}
		
		if ([cls isSubclassOfClass:[DTInteger class]]) {
			resultObject = [DTInteger integerWithObject:json];
			break;
		}

		if ([cls isSubclassOfClass:[DTDouble class]]) {
			resultObject = [DTDouble doubleWithObject:json];
			break;
		}

		if ([cls isSubclassOfClass:[DTFloat class]]) {
			resultObject = [DTFloat floatWithObject:json];
			break;
		}
		
		if ([cls isSubclassOfClass:[DTLong class]]) {
			resultObject = [DTLong longWithObject:json];
			break;
		}
		
		// 期望返回一个普通对象。
		//
		resultObject = [self decodeObjectOfClass:cls fromJSONObject:json];
		
	} while (0);
		
	return resultObject;
}

- (NSArray *)decodeArrayWithElementClass:(Class)cls fromJSONObject:(id)json
{
	// 参数验证，JSON 对象必须是一个数组对象。
	//
	if (json == nil || ![json isKindOfClass:[NSArray class]]) {
		return nil;
	}
	
	NSArray *array = (NSArray *)json;
	
	// 创建一个可变的数组，这个数组的元素的类型是由参数 cls 指定。
	NSMutableArray *resultArray = [NSMutableArray arrayWithCapacity:[array count]];
	
	if (resultArray) {
		
		// 递归的反序列化数组中的元素。
		//
		for (NSDictionary *itemDict in array) {
			id object = [self decodeWithClass:cls elementClass:nil fromJSONObject:itemDict];
			if (object) {
				[resultArray addObject:object];
			}
		}
	}
	
	return resultArray;
}

- (id)decodeDictionaryWithElementClass:(Class)cls fromJSONObject:(id)json
{
	// Invalidate the parameter.
	//
	if (![json isKindOfClass:[NSDictionary class]]) {
		[NSException raise:NSClassCastExpcetion format:@"The parameter 'object' MUST be a subclass of NSDictionary."];
		return nil;
	}
		
	// 实例化一个字典对象。
	//
	NSMutableDictionary *resultDict = [NSMutableDictionary dictionary];
	
	// 遍历字典中的字段，
	//
	for (NSString *key in [json allKeys]) {
		id object = [json objectForKey:key];
		object = [self decodeWithClass:cls elementClass:nil fromJSONObject:object];
		[resultDict setObject:object forKey:key];
	}
	
	return resultDict;
}

- (id)decodeObjectOfClass:(Class)cls fromJSONObject:(NSDictionary *)json
{
	// 参数验证。
	//
	if (json == nil) {
		[NSException raise:NSInvalidArgumentException format:@"The parameter json is nil."];
		return nil;
	}
	
	// 实例化一个由 cls 指定的类。
	//
    if (cls == [NSNumber class]){
        if ([json isKindOfClass:[NSNumber class]])
            return json;
    }
    
	id resultObject = [[cls alloc] init];
	if (resultObject == nil) {
		[NSException raise:NSMallocException format:@"The class \"%@\" cannot be initialized.", NSStringFromClass(cls)];
		return nil;
	}
		
	// 递归地初始化对象中的各个属性。
	//
	[self setupObject:resultObject ofClass:cls lookupDictionary:json];
	
	return resultObject;
}


- (void)setupObject:(id)object ofClass:(Class)cls lookupDictionary:(NSDictionary *)dict
{
	NSAssert(object != nil && cls != NULL, nil);
	
	// 优先在父类中查找属性
	//
	Class superClass = class_getSuperclass(cls);
	if (superClass != [NSObject class]) {
		[self setupObject:object ofClass:superClass lookupDictionary:dict];
	}
	
	// 获取指定类型中定义的属性的列表和数量
	//
	uint32_t count = 0;
	objc_property_t *propertyList = class_copyPropertyList(cls, &count);
	
	for (NSInteger i = 0; i < count; ++i) {
		objc_property_t property = propertyList[i];
		[self setProperty:property ofObject:object lookupDictionary:dict];
	}
	
	if (propertyList) {
		free(propertyList);
	}
}

- (void)setProperty:(objc_property_t)property ofObject:(id)object lookupDictionary:(NSDictionary *)dict
{
	NSAssert(property != NULL && object != nil, @"The parameter is invalid.");
	
	NSString *name = [NSString stringWithUTF8String:property_getName(property)];

    if (dict == (id)[NSNull null]) {
        [object setValue:[NSNull null] forKey:name];
        return;
    }
    
    if (![dict isKindOfClass:[NSDictionary class]])
        return;
    
	id value = [dict objectForKey:name];
	NSString *type = [DTJsonHelper typeStringForProperty:property];
	if (type == nil || type.length == 0 || value == nil) {
		return;
	}
	
	if ([value isKindOfClass:[NSNull class]]) {
		return;
	}
	
	if (type.length == 1) {
		if ([value isKindOfClass:[NSNumber class]]) {
			[object setValue:value forKey:name];
			return;
		}
		if (![value isKindOfClass:[NSString class]]) {
			return;
		}
		NSInteger tag = [type characterAtIndex:0];
		switch (tag) {
			case 'c': [object setValue:[NSNumber numberWithChar:[value intValue]] forKey:name]; break;
			case 'C': [object setValue:[NSNumber numberWithUnsignedChar:[value intValue]] forKey:name]; break;
			case 's': [object setValue:[NSNumber numberWithShort:[value intValue]] forKey:name]; break;
			case 'S': [object setValue:[NSNumber numberWithUnsignedShort:[value intValue]] forKey:name]; break;
			case 'i': [object setValue:[NSNumber numberWithInt:[value intValue]] forKey:name]; break;
			case 'I': [object setValue:[NSNumber numberWithUnsignedInt:[value unsignedIntValue]] forKey:name]; break;
			case 'l': [object setValue:[NSNumber numberWithInt:[value intValue]] forKey:name]; break;
			case 'L': [object setValue:[NSNumber numberWithUnsignedLong:[value unsignedIntValue]] forKey:name]; break;
			case 'q': [object setValue:[NSNumber numberWithLongLong:atoll([value UTF8String])] forKey:name]; break;
			case 'Q': [object setValue:[NSNumber numberWithUnsignedLongLong:atoll([value UTF8String])] forKey:name]; break;
			case 'f': [object setValue:[NSNumber numberWithFloat:atof([value UTF8String])] forKey:name]; break;
			case 'd': [object setValue:[NSNumber numberWithDouble:atof([value UTF8String])] forKey:name]; break;
			default:
				break;
		}
		return;
	}
//	else if ([type isEqualToString:@"@\"NSString\""]) {
//		[self setStringValue:value toProperty:name ofObject:object];
//	}
//	else if ([type isEqualToString:@"@\"NSDate\""]) {
//		[self setDateValue:value toProperty:name ofObject:object];
//	}
	else if ([type isEqualToString:@"@\"NSArray\""]) {
		NSArray *array = [dict objectForKey:name];
		Class cls = [DTJsonHelper elementClassForContainer:name ofObject:object];
		if (cls != NULL) {
			id propertyValue = [self decodeArrayWithElementClass:cls fromJSONObject:array];
			[object setValue:propertyValue forKey:name];
		}
	}
	else if ([type isEqualToString:@"@\"NSDictionary\""]) {
		NSDictionary *dictValue = [dict objectForKey:name];
		Class cls = [DTJsonHelper elementClassForContainer:name ofObject:object];
		if (cls != NULL) {
			id propertyValue = [self decodeDictionaryWithElementClass:cls fromJSONObject:dictValue];
			[object setValue:propertyValue forKey:name];
		}
	}
	else if ([type hasPrefix:@"@\""] && [type hasSuffix:@"\""]) {
		type = [type substringFromIndex:2];
		type = [type substringToIndex:type.length - 1];
		Class cls = NSClassFromString(type);
//		Class elementClass = [DTJsonHelper elementClassForContainer:name ofObject:object];
		if (cls) {
			id propertyValue = [self decodeWithClass:cls elementClass:nil fromJSONObject:value];
			[object setValue:propertyValue forKey:name];
		}
	}

}

- (void)setNumberValue:(id)value toProperty:(NSString *)name ofObject:(id)object convertSelector:(SEL)selector
{
	if ([value isKindOfClass:[NSNumber class]]) {
		[object setValue:value forKey:name];
	}
	else if ([value isKindOfClass:[NSString class]]) {
		[object setValue:[NSNumber numberWithInt:[value intValue]] forKey:name];
	}
	else {
		[NSException raise:NSGenericException format:@"Property type not matched."];
	}	
}

- (void)setStringValue:(id)value toProperty:(NSString *)name ofObject:(id)object
{
	if ([value isKindOfClass:[NSString class]]) {
		[object setValue:value forKey:name];
	}
	else {
		[NSException raise:NSGenericException format:@"Property type not matched."];
	}
}

- (void)setDateValue:(id)value toProperty:(NSString *)name ofObject:(id)object
{
	if ([value isKindOfClass:[NSNumber class]]) {
		NSTimeInterval timeInterval = [value doubleValue];
		[object setValue:[NSDate dateWithTimeIntervalSince1970:timeInterval] forKey:name];
	}
	else {
		[NSException raise:NSGenericException format:@"Property type not matched."];
	}
}

@end

static Class APClassFromString(NSString *string)
{
	if ([string hasPrefix:@"@\""] && [string hasSuffix:@"\""]) {
		string = [string substringFromIndex:2];
		string = [string substringToIndex:string.length - 1];
	}
	return NSClassFromString(string);
}
