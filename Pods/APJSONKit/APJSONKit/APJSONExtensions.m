//
//  APJSONExtensions.m
//  APJSONKit
//
//  Created by WenBi on 14-1-17.
//  Copyright (c) 2014年 WenBi. All rights reserved.
//

#import "APJSONKit.h"

@implementation NSObject (APJSONKitExtensions)

- (NSString *)JSONString
{
    NSString* result = nil;
    @try
    {
        NSError* error = nil;
        NSData* data = [NSJSONSerialization dataWithJSONObject:self options:0 error:&error];
        if (!data)
            NSLog(@"-JSONRepresentation failed. Error is: %@", error);
        else
            result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    @catch (NSException *exception)
    {
        NSLog(@"-JSONRepresentation failed. Exception: %@", exception);
    }
    return result;
}

- (NSString *)JSONRepresentation
{
    return [self JSONString];
}

@end

@implementation NSString (APJSONKitExtensions)

- (id)JSONValue
{
    NSData* data = [self dataUsingEncoding:NSUTF8StringEncoding];
    if (data)
        return [data JSONValue];
    else
        return nil;
}

@end

@implementation NSData (APJSONKitExtensions)

- (id)JSONValue
{
    id result = nil;
    @try
    {
        NSError* error = nil;
        result = [NSJSONSerialization JSONObjectWithData:self options:0 error:&error];
        if (result == nil)
            NSLog(@"-JSONValue failed. Error is: %@", error);
    }
    @catch (NSException *exception)
    {
        NSLog(@"-JSONValue failed. Exception: %@", exception);
    }
    return result;
}

@end