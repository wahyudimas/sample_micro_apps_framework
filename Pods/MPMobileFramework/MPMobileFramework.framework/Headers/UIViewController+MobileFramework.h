//
//  UIViewController+MobileFramework.h
//  APMobileFramework
//
//  Created by tashigaofei on 14-6-30.
//  Copyright (c) 2014年 Alipay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTMicroApplication.h"

@interface UIViewController (MobileFramework)

// ViewController所属的App。
@property (nonatomic, weak) DTMicroApplication *microApplication;

@end
