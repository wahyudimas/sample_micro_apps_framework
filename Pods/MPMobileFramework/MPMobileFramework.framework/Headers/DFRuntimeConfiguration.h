//
//  DFRuntimeConfiguration.h
//  MPMobileFramework
//
//  Created by shenmo on 5/7/15.
//  Copyright (c) 2015 shenmo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DFRuntimeConfiguration : NSObject

+ (NSDictionary*)configuration;

@end
