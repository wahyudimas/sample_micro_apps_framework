//
//  DTBaseViewController.h
//  Alipay Mobile Common
//
//  Created by WenBi on 13-3-30.
//  Copyright (c) 2013年 Alipay. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DTMicroApplication;
#ifndef BUILD_FOR_MPAAS
@class DTRpcAsyncCaller;
#endif

typedef NS_ENUM(NSUInteger, kViewAppearStateType) {
    kViewAppearStateTypeNone = 0,
    kViewAppearStateTypeWillAppear,
    kViewAppearStateTypeDidAppear
};

/**
 * 所有业务应用的视图控制器的基类。
 */
@interface DTBaseViewController : UIViewController <UIGestureRecognizerDelegate>

/** 获取当前 view controller 所在的 app 对象。 */
@property(nonatomic, assign, readonly) DTMicroApplication *application;

@property (nonatomic, assign) BOOL isMiddlePage;
@property (nonatomic, assign) BOOL multiLayer;
@property (nonatomic, strong) UIImage *preControllerImage;

@property (nonatomic, assign) kViewAppearStateType viewAppearState;
@property (nonatomic, assign) BOOL is3DTouchLaunch; // 标示是3DTouch启动VC

/**
 * Pops the receive from the navigation controller.
 */
- (void)back;

#ifdef BUILD_FOR_MPAAS
/**
 *  执行RPC
 *
 *  @return 如果没有RPC模块，这个接口是不会执行的，返回nil。
 */
- (id)callAsyncBlock:(void (^)(void))block completion:(void (^)(void))completion;
#else
- (DTRpcAsyncCaller *)callAsyncBlock:(void (^)(void))block completion:(void (^)(void))completion;
#endif
- (void)stopAllAsyncBlock;

/**
 *  返回页面的类名
 */
- (NSString *)pageID;

/**
 *  这个DTViewController是否要自动隐藏navigationBar，默认为NO。业务某个ViewController需要隐藏NavigationBar可以重载此方法并返回YES.
 */
- (BOOL)autohideNavigationBar;

/**
 *  如果某个viewcontroller希望自己的titlebar是不透明，并且指定一个颜色，可以重写这个方法，并返回希望的颜色。
 *  仅限于被Push的VC，tabbar里的VC还是不允许修改navigationBar的半透明属性
 */
- (UIColor *)opaqueNavigationBarColor;

/**
 * Called just before release the controller's view from memory.
 *
 * Regardless of clicking back button or popping gesture, this method is 
 * always reachable.
 */
- (void)viewWillDestroy;

/**
 * 展示title边上的小菊花
 * 注意：标题字数少于5个使用
 */
- (void)startTitleLoading;

/**
 * 消失title边上的小菊花
 * 注意：标题字数少于5个使用
 */
- (void)stopTitleLoading;

/**
 * 显示navigationbar返回按钮title
 *
 * 注意：
 * 1、默认显示规则：二级页面显示一级页面的title；二级以上页面显示“返回”
 * 2、不使用默认显示规则，可以重写该方法
 *
 */
- (void)displayBackButtonTitle;

/**
 *  是否在viewWillDisappear里自动清除所有APActionSheet控件。
 *  注意：默认是返回YES，如果不想自动清除，就重写该方法返回NO。
 *
 *  @return 自动清除返回YES，否则返回NO。
 */
- (BOOL)autoDismissAPActionSheets;

/**
 * 清除主窗口（window）中所有的弹出框（UIAlertView）。
 * 这个清除操作，并不会导致弹出框委拖的调用，也就是说，使用该方法清除弹出框，弹出框的委拖接收不到任何回调。
 */
+ (void)dismissAlertViews;

/**
 *  App语言设置修改时调用该方法. 子类在语言设置改变时需要做更新布局、更新资源文件等, 实现此方法.
 *
 *  @param info 语言设置修改时的附加信息.内容格式为 @{APLanguageSettingInfoOldKey:@"en",APLanguageSettingInfoNewKey:@"zh-Hans"};
 *
 */
- (void)languageSettingDidChange:(NSDictionary *)info   NS_REQUIRES_SUPER ;

@end
