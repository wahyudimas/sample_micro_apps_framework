//
//  DFClientDelegate.h
//  Alipay Mobile Runtime Framework
//
//  Created by WenBi on 13-3-30.
//  Copyright (c) 2013年 Alipay. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef BUILD_FOR_MPAAS
//语音转文字APPKey，分别为搜索、聊天和生产的Key
static const char *SpeechRecognizerAPPKeys[3] = {"21812028", "21812030", "21815038"};
//语音转文字APPSecretKey，分别为搜索、聊天和生产的Key
static const char *SpeechRecognizerAPPSecretKeys[3] = {"3c9b96535cdae69a36c22d34d22abfa3",
                                                                                "45c3aa08aca1259bc65ef80a3e291d6e",
                                                                                "713ac7fc458b04b838538a745187fb3b"};
//语音转文字服务器地址
static const char *SpeechRecognizerServiceURL = "ws://speechapi.m.taobao.com/websocket";
#endif

@class DFContext;

/**
 * <code>DFClientDelegate</code> 用于处理框架应用程序的生命周期及运行时的处理（如内存警告等）。
 */
@interface DFClientDelegate : UIResponder <UIApplicationDelegate>//, DFSplashScreenDelegate>

/**
 * 获取应用程序对象的唯一的委托对象。
 */
+ (DFClientDelegate *)sharedDelegate;

/**
 * 获取或设置框架上下文对象。
 */
@property(nonatomic, strong) DFContext *context;

/**
 * 应用主窗口
 */
@property(nonatomic, strong) UIWindow *window;

/**
 * 启动钱包底层处理模块
 */
- (BOOL)startBootLoaderModule;

@end
