//
//  DFContext.h
//  Alipay Mobile Runtime Framework
//
//  Created by WenBi on 13-3-31.
//  Copyright (c) 2013年 Alipay. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DFMicroApplicationManager;
@class DFServiceManager;
@class DFNavigationBarDelegate;

@interface DFContext : DTContext

@property(nonatomic, strong) UIWindow *window;
@property(nonatomic, strong) UINavigationController *navigationController;
@property(nonatomic, strong) DFMicroApplicationManager *applicationManager;
@property(nonatomic, strong) DFServiceManager *serviceManager;
@property(nonatomic, strong) DFNavigationBarDelegate *navigationBarDelegate;

+ (DFContext *)sharedContext;

/**
 * 根据指定的名称启动另一个应用。
 *
 * @param name 要启动的应用名。
 * @param params 应动应用时，需要转递给另一个应用的参数。
 * @param animated 指定启动应用时，是否显示动画。
 *
 * @return 应用启动成功返回YES，否则返回NO。
 */
- (BOOL)startApplication:(NSString *)name params:(NSDictionary *)params animated:(BOOL)animated;

- (BOOL)startApplication:(NSString *)name params:(NSDictionary *)params launchMode:(DTMicroApplicationLaunchMode)launchMode;

- (BOOL)startApplication:(NSString *)name params:(NSDictionary *)params launchMode:(DTMicroApplicationLaunchMode)launchMode sourceId:(NSString *)sourceId;

/**
 * 根据指定的名称查到一个服务。
 *
 * @param name 服务名
 *
 * @return 如果找到指定名称的服务，则返回一个服务对象，否则返回空。
 */
- (id)findServiceByName:(NSString *)name;

/**
 * 注册一个服务。
 *
 * @param name 服务名
 */
- (BOOL)registerService:(id)service forName:(NSString *)name;

/**
 * 反注册一个已存在的服务。
 *
 * @param name 服务名。
 */
- (void)unregisterServiceForName:(NSString *)name;

/**
 * 开始事务处理。
 *
 * @return 如果正在事务处理返回NO，否则返回YES。
 */
- (BOOL)beginAppTransaction;

/**
 * 提交事务处理。
 */
- (void)commitAppTransaction;

/**
 * 查找指定App关联的所有ViewController。
 *
 * @param name 指定Application的名字。
 *
 * @return 如果指定的应用有关联的ViewController，则全部返回，否则返回nil。
 */
- (NSArray *)viewControllersOfApplication:(NSString *)name;

/**
 * 设置指定Application关联的所有ViewController。
 *
 * @param viewControllers 关联的所有ViewController。
 * @param name            要查找的应用名。
 * @param animated        是否带动画。
 *
 */
- (void)setViewControllers:(NSArray *)viewControllers forApplication:(NSString *)name animated:(BOOL)animated;

/**
 * 根据指定的名称判断是否可以启动一个应用。
 *
 * @param name 要启动的应用名。
 * @param params 应动应用时，需要转递给另一个应用的参数。
 *
 * @return 可以启动返回YES，否则返回NO。
 */
- (BOOL)canHandleStartApplication:(NSString *)name params:(NSDictionary *)params;

@end