//
//  APLogAddions.h
//  APRemoteLogging
//
//  Created by tashigaofei on 15/10/13.
//  Copyright © 2015年 Alipay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APLogAdditions : NSObject
@property(nonatomic, strong) NSString *UUID;
@property(nonatomic, strong) NSString *UTDID;
@property(nonatomic, strong) NSString *clientID;
@property(nonatomic, strong) NSString *deviceModel;
@property(nonatomic, strong) NSString *userID;
@property(nonatomic, strong) NSString *logServerURL;
@property(nonatomic, strong) NSString *configServerURL;
@property(nonatomic, strong) NSString *language;

+(instancetype) sharedInstance;
@end