//
//  MPLog.h
//  MPLog
//
//  Created by shenmo on 1/4/15.
//  Copyright (c) 2015 shenmo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MPLog.
FOUNDATION_EXPORT double MPLogVersionNumber;

//! Project version string for MPLog.
FOUNDATION_EXPORT const unsigned char MPLogVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MPLog/PublicHeader.h>

#import "APLog.h"
#import "APFileLog.h"