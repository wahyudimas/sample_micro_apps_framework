//
//  DTReachability.m
//  APMobileNetwork
//
//  Created by 朱建 on 13-4-10.
//  Copyright (c) 2013年 Alipay. All rights reserved.
//

#import "DTReachability.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

static CTTelephonyNetworkInfo *telephonyInfo;

@interface DTReachability ()

@property(nonatomic, strong) DTNetReachability *internetReach;

@end

@implementation DTReachability

+ (DTReachability *)sharedDTReachability
{
    static dispatch_once_t once;
    static DTReachability *instance;
    dispatch_once(&once, ^
    {
        instance = self.new;

        [[NSNotificationCenter defaultCenter] addObserver:instance selector:@selector(reachabilityChanged:) name: kDTReachabilityChangedNotification object: nil];
        
        instance.internetReach = [DTNetReachability reachabilityForInternetConnection];
        [instance.internetReach startNotifier];
        [instance updateWithReachability: instance.internetReach];
        
        telephonyInfo = [[CTTelephonyNetworkInfo alloc] init];
        
    });
    return instance;
}

- (void)reachabilityChanged:(NSNotification *)note
{
    DTNetReachability *curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[DTNetReachability class]]);
    [self updateWithReachability:curReach];
}

- (void)updateWithReachability:(DTNetReachability *)curReach
{
    if (curReach == self.internetReach)
    {
        self.networkStatus = [curReach currentReachabilityStatus];
    }
}

- (void)refreshReachability
{
    self.networkStatus = [self.internetReach currentReachabilityStatus];
}

- (BOOL)isReachableVia3G
{
    return [self.internetReach isReachableVia3G];
}

- (BOOL)isReachableVia2G
{
    return [self.internetReach isReachableVia2G];
}

- (CTTelephonyNetworkInfo *)telephonyInfo
{
    return telephonyInfo;
}

+ (NSString *)networkName {
    
    NetworkStatus networkStatus = [DTReachability sharedDTReachability].networkStatus;
    if (networkStatus == ReachableViaWiFi) {
        return @"WIFI";
    } else if (networkStatus == ReachableViaWWAN) {
        if ([telephonyInfo respondsToSelector:@selector(currentRadioAccessTechnology)]) { // ios7及以上
            if ([[telephonyInfo currentRadioAccessTechnology] isEqualToString:CTRadioAccessTechnologyGPRS]) {
                return @"GPRS";
            } else if ([[telephonyInfo currentRadioAccessTechnology]  isEqualToString:CTRadioAccessTechnologyEdge]) {
                return @"EDGE";
            } else if ([[telephonyInfo currentRadioAccessTechnology]  isEqualToString:CTRadioAccessTechnologyWCDMA]) {
                return @"WCDMA";
            } else if ([[telephonyInfo currentRadioAccessTechnology]  isEqualToString:CTRadioAccessTechnologyHSDPA]) {
                return @"HSDPA";
            } else if ([[telephonyInfo currentRadioAccessTechnology]  isEqualToString:CTRadioAccessTechnologyHSUPA]) {
                return @"HSUPA";
            } else if ([[telephonyInfo currentRadioAccessTechnology]  isEqualToString:CTRadioAccessTechnologyCDMA1x]) {
                return @"CDMA1X";
            } else if ([[telephonyInfo currentRadioAccessTechnology]  isEqualToString:CTRadioAccessTechnologyCDMAEVDORev0]) {
                return @"CDMAEVDOREV0";
            } else if ([[telephonyInfo currentRadioAccessTechnology]  isEqualToString:CTRadioAccessTechnologyCDMAEVDORevA]) {
                return @"CDMAEVDOREVA";
            } else if ([[telephonyInfo currentRadioAccessTechnology]  isEqualToString:CTRadioAccessTechnologyCDMAEVDORevB]) {
                return @"CDMAEVDOREVB";
            } else if ([[telephonyInfo currentRadioAccessTechnology]  isEqualToString:CTRadioAccessTechnologyeHRPD]) {
                return @"HRPD";
            } else if ([[telephonyInfo currentRadioAccessTechnology]  isEqualToString:CTRadioAccessTechnologyLTE]) {
                return @"LTE";
            }
            return @"UNKNOWN";
        } else {// ios7以下
            return @"WWAN";
        }
    } else {
        return @"NotReachable";
    }
}

@end
