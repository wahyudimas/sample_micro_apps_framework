//
//  SampleAppDelegate.swift
//  Sample_Micro_Apps_Framework
//
//  Created by Wahyudimas Hutama Rachman Syarief on 6/15/17.
//  Copyright © 2017 Wahyudimas Hutama Rachman Syarief. All rights reserved.
//

import UIKit

@objc(SampleAppDelegate)
class SampleAppDelegate: NSObject, DTMicroApplicationDelegate {
	func rootController(in application: DTMicroApplication!) -> UIViewController! {
		return SampleViewController()
	}
}
