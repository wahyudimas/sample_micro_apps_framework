//
//  Sample_Micro_Apps_Framework.h
//  Sample_Micro_Apps_Framework
//
//  Created by Wahyudimas Hutama Rachman Syarief on 6/15/17.
//  Copyright © 2017 Wahyudimas Hutama Rachman Syarief. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Sample_Micro_Apps_Framework.
FOUNDATION_EXPORT double Sample_Micro_Apps_FrameworkVersionNumber;

//! Project version string for Sample_Micro_Apps_Framework.
FOUNDATION_EXPORT const unsigned char Sample_Micro_Apps_FrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Sample_Micro_Apps_Framework/PublicHeader.h>

#import <MPMobileFramework/MPMobileFramework.h>
